package rag.advanced.cucumber.steps;


import java.util.ArrayList;
import java.util.List;

import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import rag.advanced.cucumber.utils.BaseUtil;
import rag.advanced.cucumber.utils.Transformation.DhsToDollarTransform;
import rag.advanced.cucumber.utils.Transformation.EmailTransform;

public class LoginStep {
	BaseUtil base;
	
	public LoginStep(BaseUtil m) {
		base=m;
	}
	
	@Given("^I navigate to login page$")
	public void i_navigate_to_login_page() throws Throwable {
	    System.out.println("______I navigate to login page");
	    System.out.println("______my browser "+base.getBrowser());
	    
	}
	
	@Given("^I entered username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void i_entered_username_and_password(String arg1, String arg2) throws Throwable {
		 System.out.println("___username "+arg1+"___password "+arg2);
	}
	
	@Given("^I entered the email as Email:([^\"]*)$")
	public void i_entered_the_email_as(@Transform(EmailTransform.class)String email) {
		 System.out.println("__email___"+email);
		
	}
	@Given("^I entered salary (\\d+)$")
	public void i_entered_salary(@Transform(DhsToDollarTransform.class)int salary) {
		System.out.println("__salary___"+salary);
	}
	
	@Given("^I entered the following$")
	public void i_entered_the_following(DataTable myData) {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)

		List<LoginStep.User> users=new ArrayList<LoginStep.User>();
		 users=myData.asList(User.class);
		for(User user:users) {
			System.out.println(user.toString());
		}

	}

	@Given("^I click login button$")
	puccbb  void i_click_login_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("I click login button");
	    
	}

	@Then("^I should see the userform page$")
	public void i_should_see_the_userform_page() throws Throwable {
		System.out.println("I should see the userform page");
	   
	}

public class User{
	String firstName;
	String lastName;
	int age;
	
	public User(String firstName,String lastName,int age) {
		this.firstName=firstName;
		this.lastName=lastName;
		this.age=age;
	}
	
	public String toString() {
		return firstName+" , "+lastName+ " , "+age;
	}
}

}
