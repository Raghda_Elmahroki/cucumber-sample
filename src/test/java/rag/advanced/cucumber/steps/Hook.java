package rag.advanced.cucumber.steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import rag.advanced.cucumber.utils.BaseUtil;

public class Hook {

	BaseUtil base;
	
	public Hook(BaseUtil myBase) {
		base=myBase;
	}
	@Before
	public void InitializeTest() {
		System.out.println("Initialize Tesr");
		base.setBrowser("FireFox");
	//	base.setDriver(new FirefoxDriver());
	}
	
	@After
	public void TearTest() {
		System.out.println("Tear Test");
	}
}
