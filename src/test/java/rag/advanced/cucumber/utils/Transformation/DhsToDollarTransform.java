package rag.advanced.cucumber.utils.Transformation;

import cucumber.api.Transformer;

public class DhsToDollarTransform extends Transformer<Integer>{

	@Override
	public Integer transform(String value) {
		
		return (int)(Integer.valueOf(value)/3);
	}

}
