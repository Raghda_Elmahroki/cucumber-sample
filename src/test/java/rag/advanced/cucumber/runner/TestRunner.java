package rag.advanced.cucumber.runner;


 
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
 
@CucumberOptions(features = "src/test/resources/features/Login.feature",
        glue = "rag.advanced.cucumber.steps")
public class TestRunner extends AbstractTestNGCucumberTests {
}