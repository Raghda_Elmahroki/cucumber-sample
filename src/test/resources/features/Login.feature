Feature: Login Feature
This feature deals with login functionality of the application

Scenario: Login with correct username and password
Given I navigate to login page
And I entered username "admin" and password "admin"
And I entered the email as Email:admin
And I entered salary 1000
And I entered the following
|firstName  |lastName  |age|
|Raghda |Elmahroki |33 |
|Rana   |Ismail    |29 |
And I click login button
Then I should see the userform page