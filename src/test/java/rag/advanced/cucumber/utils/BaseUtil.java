package rag.advanced.cucumber.utils;

import org.openqa.selenium.WebDriver;

public class BaseUtil {
 private String browser;
 private WebDriver driver;
 

public String getBrowser() {
	return browser;
}

public void setBrowser(String browser) {
	this.browser = browser;
}


public void setDriver(WebDriver myDriver) {
	driver=myDriver;
}
 
}
